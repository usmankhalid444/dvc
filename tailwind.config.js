/** @format */

module.exports = {
	purge: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
	mode: 'jit',
	darkMode: false, // or 'media' or 'class'
	theme: {
		extend: {
			fontFamily: {
				'display': ['Poppins',],
				'body': ['Arial Narrow',],

			   },
			colors: {
				// personal
				custom: {
					light0: '#FCFAF4',
					light2: '#F7F2E0',
					blue1: '#6ba3d4',
					blue2: '#337ab7',
					blue3: '#23527c',
					blue4: '#1f4e79',
					blue5: '#203864',
					blue6:"#214F73",
					blue7:"#478BCA",
					primary: '#063561',
					secondary1: '#0f4173',
					secondary2: '#012952',
					orange1: 'orange',
					orange2: '#ea851c',
					orange3: '#cc3300',
					orange4: '#df5c2e',
					orange5:'#EA851D',
					light1: '#fcfaf4',
					grey1: '#d2d6dc',
					green1: '#619235',
					bg1: '#f7f2e0',
					darkcream: '#f7f2e0',
					lightcream: '#fcfaf4',
					hd1: '#bfd4ea',
					thead1: 'rgb(56,103,155)',
					coral: 'coral',
					lightgreen: '#8BC1AF',
					darkgreen: '#45907b',
				},
			},

			fontSize: {
				pTitle: ['2.625rem', '2.88rem'],
				pSubTitle: ['1.875rem', '2.0625rem'],
			},
		},
	},
	variants: {
		extend: {},
	},
	plugins: [],
};

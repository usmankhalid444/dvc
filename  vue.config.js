module.exports = {
  publicPath: './',
  chainWebpack: config => {
    config.module.rule('images').use('url-loader')
      .loader('file-loader') // replaces the url-loader
      .tap(options => Object.assign(options, {
        name: 'images/[name].[hash:8].[ext]'
      }))
    config.module.rule('svg').use('file-loader')
      .tap(options => Object.assign(options, {
        name: 'images/[name].[hash:8].[ext]'
      }))
    },
};
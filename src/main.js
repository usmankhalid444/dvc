import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faCoffee } from '@fortawesome/free-solid-svg-icons'
import { faBriefcase,faAddressCard } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import Calendar from 'v-calendar/lib/components/calendar.umd'
import DatePicker from 'v-calendar/lib/components/date-picker.umd'
import VueApexCharts from 'vue-apexcharts'
Vue.use(VueApexCharts)

Vue.component('apexchart', VueApexCharts)
Vue.component('calendar', Calendar)
Vue.component('date-picker', DatePicker)

library.add(faAddressCard)
library.add(faBriefcase)
library.add(faCoffee)
Vue.component('font-awesome-icon', FontAwesomeIcon)
import VCalendar from 'v-calendar';

// Use v-calendar & v-date-picker components
Vue.use(VCalendar, {
  componentPrefix: 'vc',  // ther defaults
});
import router from './router' 
Vue.use(VueRouter)
import './index.css'
let vm = new Vue({
  router,
  render: h => h(App)
}).$mount('#app')

window.vm = vm
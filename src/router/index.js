import VueRouter from "vue-router";
import Home from "../pages/all-resorts";
import Faq from "../pages/buying/faq-buying/index.vue";
import Calculator from "../pages/buying/calculator/index.vue";
import RentingDVC from "../pages/buying/rental-dvc-points/index.vue";
import Buying from "../pages/buying";
import DeedExpiration from "../pages/buying/deed-expiration/index.vue";
import AnnualDues from "../pages/buying/annual-dues/index.vue";
import RetailPrices from "../pages/buying/retail_prices";
import RecentSales from "../pages/buying/recent-sales/index.vue";
import ComparePrices from "../pages/buying/compare-prices/index.vue";
import BuyingWork from "../pages/buying/buying-work/index.vue";
import Financing from "../pages/buying/financing/index.vue";
import ProductChecklist from "../pages/buying/product-checklist/index.vue";
import HelpFamilies from "../pages/buying/help-families/index.vue";
import Testimonials from "../pages/buying/testimonials/index.vue";
import ResortOverview from "../pages/all-resorts/index.vue";
import CreatList from "../pages/buying/create-list/index.vue";
import AboutUs from "../pages/buying/about-us/index.vue";
import Demo from "../pages/demo/index.vue";
const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/buying",
    name: "Buying",
    component: Buying,
    children: [
      {
        name: "faq",
        path: "faq",
        component: Faq,
      },
      {
        name: "calculator",
        path: "calculator",
        component: Calculator,
      },
      {
        name: "rentingDVC",
        path: "rentingDVC",
        component: RentingDVC,
      },
      {
        name: "deed exp",
        path: "deed-expiration",
        component: DeedExpiration,
      },
      {
        name: "annual dues",
        path: "annual-dues",
        component: AnnualDues,
      },
      {
        name: "retail prices",
        path: "retail-prices",
        component: RetailPrices,
      },
      {
        name: "recent sales",
        path: "recent-sales",
        component: RecentSales,
      },
      {
        name: "compare prices",
        path: "compare-prices",
        component: ComparePrices,
      },
      {
        name: "buying work",
        path: "buying-work",
        component: BuyingWork,
      },
      {
        name: "financing",
        path: "financing",
        component: Financing,
      },
      {
        name: "product checklist",
        path: "product-checklist",
        component: ProductChecklist,
      },
      {
        name: "help families",
        path: "help-families",
        component: HelpFamilies,
      },
      {
        name: "testimonials",
        path: "testimonials",
        component: Testimonials,
      },
      {
        name: "resortOverview",
        path:"resortOverview",
        component:ResortOverview
      },
      {
        name:"createlist",
        path:"createlist",
        component :CreatList
      },
      {
        name:'aboutus',
        path:'aboutus',
        component:AboutUs
      }
    ],
  },
  {
    name: "demo",
    path: "/demo",
    component: Demo,
  },
];
const router = new VueRouter({
  mode: "history",
  base: process.env.VUE_APP_BASE_URL,
  routes,
});

export default router;
